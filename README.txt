        
        
        
        GUI - Graphical User Interface
        Provided by http://vision-media.ca
        Developed by Tj Holowaychuk
        
        ------------------------------------------------------------------------------- 
        INSTALLATION
        ------------------------------------------------------------------------------- 
        
        Simple enable jquery_update, jquery_ui, and this module.
        
        This module is an API and will not provide any immediate functionality unless
        associated modules are installed, and enabled.
        
        ------------------------------------------------------------------------------- 
        PERMISSIONS
        ------------------------------------------------------------------------------- 

        administer gui components
          - used by GUI component related modules as generic administration permission.
          
        administer gui skins
          - allows users to choose which skin to use per GUI component.  
   
        ------------------------------------------------------------------------------- 
        GUI COMPONENTS
        ------------------------------------------------------------------------------- 
  
        A GUI Component is simply a displayable piece of content which often offers
        interactivity between the system and the user. Below are known GUI Component
        modules:
          - gui_tabs_component

	      ------------------------------------------------------------------------------- 
	      USING COMPONENTS
	      -------------------------------------------------------------------------------
	
				All components should provide a 'render' method, and each module using components
				should do so using the 'gui_get_component' function. See tab example below:
				
					$tabs =& gui_get_component('tabs');
					$tabs->add_tab('Welcome', 'Contents of welcome here');
					$tabs->add_tab('Contact', 'Contents of contact here');
					echo $tabs->render();
				
				Alternatively many components may have additional render methods allowing specific
				portions to be rendered. For example the tabs component allows you to render
				the tabs themselves, and tab bodies separately so they provide more flexibly in 
				terms of theming.
	
        ------------------------------------------------------------------------------- 
        CREATING A COMPONENT
        -------------------------------------------------------------------------------
        
        To create a component you should implement both hook_gui_skins() as well as 
        hook_gui_components() which will essentially register the look and functionality
        of your component. Each component must extend the abstract GUI_Component class
        which should be located within 'mycomponent.inc' by default. Each component
        must provide a default skin, optionally providing additional.
       
				CONVENTIONS
				
        The directory structure of your module does not have to follow this structure
        however unless explicitly stated file paths will be assumed for gathering skins,
        component includes etc. 
        - mycomponent/
        	- skins/
            - default/
              - images/
              - default.css
          - mycomponent.inc
          - mycomponent.module
          - mycomponent.info

        Naming conventions for extending the GUI_Component class are as follows:
        GUI_<component>_Component. For example the 'Tabs' component implementation
        would be GUI_Tabs_Component, and the module name would be 'gui_tabs_component' 
        for additional clarity.

				JAVASCRIPT
				
				Each javascript component should consist of the bare minimum script:
				
					GUI.extend('GUI_Component', 'GUI_Tabs_Component');

					/**
				 		* Initialize.
				 		*/
					GUI_Tabs_Component.prototype.init = function() {

					};

				RENDERING
				
				Each component should override the render() method. This method should invoke
				the parent render() method via parent::render(), which finalizes settings and
				invokes hook_gui_component_alter() before rendering.
				
				Any sub render method should also invoke the parent render() method to ensure 
				that all settings and files have been added.
				
				EXAMPLES
				
				Each component should override the example() method. This should return markup
				returned which allows each component to create an instance of itself for interface
				examples when switching skins etc.

        ------------------------------------------------------------------------------- 
        CREATING SUB-COMPONENTS
        -------------------------------------------------------------------------------

        Through the use of hook_gui_components() a module may register several components
        which is useful in cases where components provide similar functionality.         
        
        ------------------------------------------------------------------------------- 
        FUTURE GOALS
        -------------------------------------------------------------------------------

        - Interface for ease of extending
        - Ability to create component instances via Ajax
        - Unification of component configuration providing helpers for choosing easing functions, durations etc
  