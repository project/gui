
var GUI = GUI || { settings: {}, components: {} };

/**
 * Create components initiated from page load.
 */
GUI.createComponents = function() {
	try {
		$.each(Drupal.settings.gui_components, function(id, info){
			pseudo_class = info['pseudo_class'];
			if (eval('typeof ' + pseudo_class) == 'function'){
				component = eval('new ' + pseudo_class + '()');
				component.id = id;
				component.name = info['name']; 
				component.settings = Drupal.settings.gui_component_settings[id] || {};
				component.element = $('#gui-component-' + id);
				component.element.addClass('processed');
				component.element.gid = id;
				GUI.registerComponent(component);
				component.init();
			}
		});
	}
	catch(e) {
		// Do nothing
	}
};

/**
 * Register a new GUI_Component object.
 *
 * @param object component
 */
GUI.registerComponent = function(component) {
	this.components[component.id] = component;
};

/**
 * Get GUI Component by id.
 *
 * @param int id
 *
 * @return object
 */
GUI.getComponentById = function(id) {
	component = this.components[id] || id;
	return component;
};

/**
 * Pseudo class inheritance.
 *
 * @param string parent
 *	 'parent' class name which is being 'extended'.
 *
 * @param string klass
 *	 'class' name.
 */
GUI.extend = function(parent, klass) {
	to_eval = klass + ' = function(){};';
	to_eval += klass + '.prototype = new ' + parent + ';';
	to_eval += klass + '.prototype.constructor = ' + klass + ';';
	eval(to_eval);
};

if (Drupal.jsEnabled){
	$(function(){
		GUI.createComponents();
	});
}

