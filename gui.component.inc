<?php

/**
 * @file
 * GUI Component abstract classed used to create custom components.
 * @author TJ Holowaychuk <tj@vision-media.ca>
 * @link http://vision-media.ca
 * @package gui
 */

/**
 * GUI_Component
 *
 * A GUI Component object is the basis of what becomes an interactive
 * graphical representation of an interface such as tabs, accordions, datagrids
 * and much more. Although you cannot instance this class you must extend it
 * when creating your own components.
 *
 * HOOKS
 *
 * The GUI_Component class issues 'hooks' in a similar manor to regular Drupal
 * hooks, however they are methods. Currently the following hooks may be implemented:
 *   - hook::add_js()
 *     Return an array of javascript filenames relative to your component module. This 
 *     is handled in this manor to allow for future Ajax instances of components to be 
 *     created.
 *
 *   - hook::add_css() 
 *     Similar to to hook::add_js() however this allows components to provide base 
 *     css structures, which then are most likely enhanced with graphics and colors
 *     using a provided skin.
 *
 *   - hook::add_settings()
 *     An associative array of settings visible to javascript. Use in place of drupal_add_js().
 *     These settings will be available from the 'settings' property of your javascript component
 *     implementation. 
 *
 * NOTES
 * 
 * When implementing a constructor function for your class you should call the parent method which
 * can be done using parent::__construct(). This ensures that GUI_Component may still perform its
 * original processing (be sure to pass $component_info to the parent as well).
 * 
 * The entire concept of the GUI_Component implementations is to essentially provide very clean
 * and simple methods of creating such interactive components. For example the tabs component may 
 * provide several methods useful for customizing and adding tabs, or a context sensative menu
 * component may allow you to add seperators or menu items.
 *
 * @todo add set_current_skin(skin)
 */
abstract class GUI_Component {
	
	/**
	 * Id.
	 *
	 * @var int
	 */
	public $id = 0;
	
	/**
	 * JavaScript filepaths.
	 *
	 * @var array
	 */
	public $js = array();
	
	/**
	 * JavaScript settings.
	 *
	 * @var array
	 */
	public $settings = array();
		
	/**
	 * CSS filepaths.
	 *
	 * @var array
	 */
	public $css = array();
	
	/**
	 * Classes.
	 *
	 * @var array
	 */
	public $classes = array();
		
	/**
	 * Implementation component info.
	 *
	 * @var array
	 */
	public $component_info = array();
	
	/**
	 * Implementation skin info.
	 *
	 * @var array
	 */
	public $skin_info = array();
	
	/**
	 * Current theme choosen.
	 *
	 * @var string
	 */
	public $current_skin = 'default';
	
	/**
	 * Module path.
	 *
	 * @var string
	 */
	public $module_path = '';
			
  /**
   * Constructor.
   */
  public function __construct($component_info) {
	  $this->component_info = $component_info;
	  $this->skin_info = $this->_get_skin_info();
	  $this->current_skin = $this->_get_current_skin();
	  $this->module_path = drupal_get_path('module', $this->component_info['module']);
		$this->_add_current_skin();
  }

  /* -----------------------------------------------------------------

    Public Methods

  ------------------------------------------------------------------ */

	/**
	 * Add classes. 
	 *
	 * This method adds additional classes to the component 
	 * wrapper div, allowing greater flexibility when styling.
	 *
	 * @param mixed $classes
	 *   Array or string of classes.
	 */
	function add_classes($classes) {
	  if (is_string($classes)){
			$this->classes[] = $classes;
		}
		elseif (is_array($classes) && !empty($classes)){
			$this->classes = array_merge($this->classes, $classes);
		}
	}
	
	/**
	 * Singular alias of add_classes().
	 */
	function add_class($class) {
	  $this->add_classes($class);
	}
	
	/**
	 * Override.
	 */
	function render() { 
		static $invoked = 0;
		if ($invoked) {
			return;
		}  
		$this->_invoke_hooks();
	  $this->_add_js();
    $this->_add_css();		
    $this->_add_settings();		
		drupal_alter('gui_component', $this);
		$invoked = 1;
	}
	
	/**
	 * Override.
	 */
	function example() {
		return t('No example available.');
	}
		
  /* -----------------------------------------------------------------

    Private Methods

  ------------------------------------------------------------------ */

	/**
   * Invoke all hooks.
   */
  private function _invoke_hooks() {
    $hooks = array('add_js' => 'js', 'add_css' => 'css', 'add_settings' => 'settings');
    foreach((array) $hooks AS $hook => $prop) {
      if (method_exists($this, $hook)){
				// Direct property assignment
				if (in_array($prop, array('settings'))){
  	    	$this->$prop = $this->$hook();
				}
				// Array mergers
				else {
					if ($js = $this->$hook()){
						$this->$prop = @array_merge($this->$prop, $js);
					}
				}
      }
    } 
  }
  
  /**
   * Set skin information.
   * 
   * @return array
   */
  private function _get_skin_info() {
    $skins = gui_invoke_skins();
    return $skins[$this->component_info['name']];
  }
  
  /**
   * Get current skin info.
   * 
   * @return array
   */
  private function _get_current_skin() { 
    return $this->skin_info[variable_get('gui_component_' . $this->component_info['name'] . '_skin', $this->component_info['default skin'])];
  }

	/**
	 * Add skin related files.
	 */
	private function _add_current_skin() {
		if (empty($this->current_skin)){
			return;
		}
	  $filepath = drupal_get_path('module', $this->current_skin['module']) . '/' . $this->current_skin['file'];
		if (file_exists($filepath)){
			drupal_add_css($filepath);
		}
	}
	
  /**
   * Add JavaSripts.
   */
  private function _add_js() { 
    if (!empty($this->js)){
	    foreach((array) $this->js AS $filepath){ 
		    drupal_add_js($this->module_path . '/' . ltrim($filepath, '/'));
	    }
    }
  }

  /**
   * Add CSS.
   */
  private function _add_css() {
    if (!empty($this->css)){
	    foreach((array) $this->css AS $filepath){
		    drupal_add_css($this->module_path . '/' . ltrim($filepath, '/'));
	    }
    }
  }

  /**
   * Add JavaScript settings.
   */
  private function _add_settings() {
    drupal_add_js(array('gui_component_settings' => array($this->id => $this->settings)), 'setting');
  }  
}
