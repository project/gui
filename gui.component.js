
/**
 * Constructor.
 */
var GUI_Component = function() {
	this.id = 0;
	this.name = '';
	this.element = null;
	this.settings = {};
};

/**
 * Query the components child elements.
 * 
 * @param string selector
 *
 * @return object
 *	 jQuery.	
 */
GUI_Component.prototype.get = function(selector) {
	return $(selector, this.element);
};

/**
 * Set active element.
 * 
 * @param string context
 *   Selector indicating context of elements, ex: '.tab', '.tab-body'.
 *
 * @param mixed element
 *   Element which should now be active. Selector or jQuery object.
 *
 * @param string klass
 *   (optional) Override the default class of 'active'.
 *
 * @return object
 *	 self.	
 */
GUI_Component.prototype.setActive = function(context, element, klass) {
	c = klass || 'active';
	this.get(context).removeClass(c);
	$(element, $(this.element, context)).addClass(c);
	return this;
};

/**
 * toString implementation.
 */
GUI_Component.prototype.toString = function() {
	return '[' + this.name + '-component: ' + this.id + ']';
};



