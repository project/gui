<?php

/**
 * @file
 * Allows users change how components operate and look.
 * @author TJ Holowaychuk <tj@vision-media.ca>
 * @link http://vision-media.ca
 * @package gui
 */

/**
 * Allow users to change which skins are applied to associated components.
 * 
 * @todo style form, tis ugly!!!
 *
 * @return array
 *  Form.
 */
function gui_component_skin_settings() {
  $form = array();
	$skins = gui_invoke_skins();

	foreach((array) $skins AS $component_name => $skins_info){
		$component =& gui_get_component($component_name); 
		$component_default_skin = $component->component_info['default skin'];
		$form['gui_component_' . $component_name . '_skin'] = array(
				 '#type' => 'select',
				 '#title' => t('@component skin', array('@component' => $component_name)),
				 '#options' => array_combine(array_keys($skins_info), array_keys($skins_info)),
				 '#default_value' => variable_get('gui_component_' . $component_name . '_skin', $component_default_skin),
			 );
		$form['gui_component_' . $component_name . '_example'] = array(
		    '#type' => 'markup',
		    '#value' => '<p class="gui-example">' . $component->example() . '</p>',
		  );
	}
	
	return system_settings_form($form);
}
